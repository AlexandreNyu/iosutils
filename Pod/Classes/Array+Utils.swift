//
//  Array+Utils.swift
//  Mon.iPhone.PF
//
//  Created by Personal Finance on 07/07/2015.
//  Copyright (c) 2015 Personal Finance. All rights reserved.
//

import Foundation

public extension Array {
    
    public mutating func removeObject<U: Equatable>(object: U) {
        var index: Int?
        for (idx, objectToCompare) in self.enumerate() {
            if let to = objectToCompare as? U {
                if object == to {
                    index = idx
                }
            }
        }
        
        if(index != nil) {
            self.removeAtIndex(index!)
        }
    }
    
}

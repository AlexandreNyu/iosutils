//
//  String+Utils.swift
//  Pods
//
//  Created by Alexandre Mommers on 07/04/16.
//
//

import UIKit

public extension String {

    public var length: Int {
        return characters.count
    }

    public func characterAtIndex(index: Int) -> Character? {
        var cur = 0
        for char in characters {
            if cur == index {
                return char
            }
            cur += 1
        }
        return nil
    }

    public var hexColor: UIColor {
        return UIColor(hex: self)
    }
}

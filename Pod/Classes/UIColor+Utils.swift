//
//  UIColor+Utils.swift
//  Mon.iPhone.PF
//
//  Created by Personal Finance on 06/07/2015.
//  Copyright (c) 2015 Personal Finance. All rights reserved.
//

import Foundation
import UIKit

public extension UIColor {
    
    /**
     * get a random color
     */
    public class func randomColor() -> UIColor {
        
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
    
    /**
     * use value between 0 and 255 instead float
     */
    public convenience init(r: Int, g:Int , b:Int , a: Int) {
        self.init(red: CGFloat(r)/255, green: CGFloat(g)/255, blue: CGFloat(b)/255, alpha: CGFloat(a)/255)
    }

    public convenience init(r32: UInt32, g32: UInt32, b32: UInt32, a32: UInt32) {
        self.init(red: CGFloat(r32)/255, green: CGFloat(g32)/255, blue: CGFloat(b32)/255, alpha: CGFloat(a32)/255)
    }

    public convenience init(hex: String) {

        let hex = hex.stringByTrimmingCharactersInSet(NSCharacterSet.alphanumericCharacterSet().invertedSet)
        var int = UInt32()
        NSScanner(string: hex).scanHexInt(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
            break
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
            break
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
            break
        default:
            (a, r, g, b) = (1, 1, 1, 0)
            break
        }

        self.init(r32: r, g32:g, b32: b, a32: a)
    }
}